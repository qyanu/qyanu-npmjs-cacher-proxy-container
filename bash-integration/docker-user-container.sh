#!/usr/bin/bash
##
# If the docker container 'npmjs-cacher-proxy' is found, it's ip-address
# is used to compute a value for the variable NPM_CONFIG_REGISTRY.
#
# Then bash-compatible commands are printed to STDOUT that will set
# and export NPM_CONFIG_REGISTRY as environment variable with the
# computed value.
#
# If no (sensible) value for NPM_CONFIG_REGISTRY could be determined,
# nothing will be printed to STDOUT.
#
# Status- and/or error-information may be printed to STDERR.
#
# Note: this script _always_ exits with status 0.
##
set -eu
CONTAINER_NAME="npmjs-cacher-proxy"
if ! docker inspect "${CONTAINER_NAME}" &>/dev/null; then
    echo "INFO: docker container not found: ${CONTAINER_NAME}" >&2
    exit 0
fi
contip="$(docker inspect \
    --format '{{.NetworkSettings.IPAddress}}' \
    "${CONTAINER_NAME}")"
if [[ -n "${contip}" ]]; then
    # TODO: there's some POSIX-shell command somewhere that would
    # print out the variable in syntax compatible to the shell,
    # regardless if it's bash or otherwise. maybe look it up and use
    # this in order to make this script also non-bash compatible.
    echo "NPM_CONFIG_REGISTRY=\"http://${contip}:4873\""
    echo "export NPM_CONFIG_REGISTRY"
fi
exit 0
