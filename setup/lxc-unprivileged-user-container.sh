#!/usr/bin/bash

##
# as the current user, create an unprivileged lxc-container
# 'npmjs-cacher-proxy'.
#
# when starting the container, the software 'verdaccio' is started
# and configured to serve a caching proxy of the npm registry
# ``https://registry.npmjs.org`` on tcp port 80.
#
#
# debian package dependency of this script:
#
#   - docker.io
#   - mmdebstrap
##
set -eu

auto_detect_apt_cacher() {
    #
    # look for local lxc- or docker-container and guess an url.
    # will output the detected url to stdout, or output nothing if not
    # found.
    # will always return 0.
    #
    if lxc-info --name apt-cacher-proxy &>/dev/null; then
        ip="$(lxc-info --name apt-cacher-proxy --ips --no-humanize \
            | head -n1)"
    elif docker container inspect --format apt-cacher-proxy &>/dev/null; then
        ip="$(docker container inspect \
            --format '{{.NetworkSettings.IPAddress}}' apt-cacher-proxy)"
    fi
    if [[ -z "${ip:-}" ]]; then
        echo "INFO: Could not find lxc-container or docker container " \
            "named 'apt-cacher-proxy', auto-detection of " \
            "APT_CACHER_URL failed." >&2
    else
        # Note: 3142 is the default port of apt-cacher
        url="http://${ip}:3142"
        # test if any response
        http_status="$(curl -si "${url}" | head -n1 || true)"
        # Note: current apt-cacher versions just answer 'HTTP/1.1 200 OK'
        # full support would be to extend the following check to support
        # any 2xx http response.
        if [[ "${http_status}" = "HTTP/1.1 200 OK" ]]; then
            echo "${url}"
        fi
    fi
    return 0
}

if [[ -z "${APT_CACHER_URL:-}" ]]; then
    APT_CACHER_URL="$(auto_detect_apt_cacher)"
fi

if [[ -z "${APT_CACHER_URL:-}" ]]; then
    echo "ERROR: required environment variable missing: APT_CACHER_URL" >&2
    echo "Hint: at least specify APT_CACHER_URL=none to disable using a cache." >&2
    exit 1
fi

CONTAINER_NAME="npmjs-cacher-proxy"
if lxc-info --name "${CONTAINER_NAME}" &>/dev/null; then
    echo "ERROR: lxc container already exists: ${CONTAINER_NAME}" >&2
    exit 1
fi

if command -v systemd-path >/dev/null; then
    # TODO: is there any general, not systemd-specific, available to
    # evaluate the effective value of XDG_DATA_HOME?
    LXC_SHARE_DIR="$(systemd-path user-shared)/lxc"
else
    # default value as per spec:
    # https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
    LXC_SHARE_DIR="${XDG_DATA_HOME:-${HOME}/.local/share}/lxc"
fi

CONTAINER_DIR="${LXC_SHARE_DIR}/${CONTAINER_NAME}"

cleanup() {
    set +e
    if [[ -n "${APTOPT_FILE}" ]] && [[ -f "${APTOPT_FILE}" ]];
    then
        rm -vf "${APTOPT_FILE}" >&2
    fi
    if [[ -n "${CUSTOMIZE_FILE}" ]] && [[ -f "${CUSTOMIZE_FILE}" ]];
    then
        rm -vf "${CUSTOMIZE_FILE}" >&2
    fi
}
trap cleanup EXIT

APTOPT_FILE="$(mktemp)"
echo "INFO: using temporary file for apt.conf: ${APTOPT_FILE}" >&2

CUSTOMIZE_FILE="$(mktemp)"
echo "INFO: using temporary file for mmdebstrap's customize hook: ${CUSTOMIZE_FILE}" >&2


cat > "${APTOPT_FILE}" <<EOF
Apt::Install-Recommends "false";
Apt::AutoRemove::RecommendsImportant "false";
Apt::AutoRemove::SuggestsImportant "false";
EOF
if [[ "${APT_CACHER_URL}" != "none" ]]; then
    cat >> "${APTOPT_FILE}" <<EOF
Acquire::http { Proxy "${APT_CACHER_URL}"; };
EOF
fi
chmod a+r "${APTOPT_FILE}"

cat > "${CUSTOMIZE_FILE}" <<"EOF1"
#!/usr/bin/bash -x
set -eu
TARGET_DIR="${1}"
cat > "${TARGET_DIR}/etc/network/interfaces.d/eth0" <<"EOF2"
allow-hotplug eth0
auto eth0
iface eth0 inet dhcp
iface eth0 inet6 auto
EOF2
chroot "${TARGET_DIR}" adduser --disabled-password --comment "" user
chroot "${TARGET_DIR}" runuser --login user <<"EOF2"
set -eux
cat > "${HOME}/.bash_aliases" <<"EOT"
export PATH="${PATH}:${HOME}/bin"
EOT
. "${HOME}/.bash_aliases"
# TOFU-trusted keys (seen on 2024-02-16 via
# https://github.com/nodejs/node?tab=readme-ov-file#release-keys):
TRUSTED_KEYS=(
    4ED778F539E3634C779C87C6D7062848A1AB005C
    141F07595B7B3FFE74309A937405533BE57C7D57
    74F12602B6F1C4E913FAA37AD3A89613643B6201
    DD792F5973C6DE52C432CBDAC77ABFA00DDBF2B7
    8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600
    C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8
    890C08DB8579162FEE0DF9DB8BEAB4DFCF555EF4
    C82FA3AE1CBEDC6BE46B9360C43CEC45C17AB93C
    108F52B48DB57BB0CC439B2997B01419BD92F80A
    A363A499291CBBC940DD62E41F10027AF002F8B0
)
wget -nv https://nodejs.org/dist/latest/SHASUMS256.txt.sig
wget -nv https://nodejs.org/dist/latest/SHASUMS256.txt
for trusted_key in "${TRUSTED_KEYS[@]}"; do
    gpg \
        --no-default-keyring \
        --keyring "nodejs-release-keys.kbx" \
        --keyserver hkps://keys.openpgp.org \
        --recv-keys "0x${trusted_key}"
done
gpgv --keyring "nodejs-release-keys.kbx" \
    SHASUMS256.txt.sig SHASUMS256.txt
chksumline="$(
    grep -Pe '^[a-f0-9]{64} .node-v[0-9.]+-linux-x64\.tar\.xz$' \
        SHASUMS256.txt)"
filename="$(echo "${chksumline}" | tail --bytes=+67)"
basename="${filename%%.tar.xz}"
echo "chksumline: ${filename}"
echo "chksumline: ${chksumline}"
wget -nv "https://nodejs.org/dist/latest/${filename}"
echo "${chksumline}" \
| sha256sum --check --strict
mkdir -p opt bin
tar --xz -xf "${filename}" -C "opt"
ln -snf "${basename}" "opt/node"
for name in npm node npx verdaccio; do
    ln -snf "../opt/node/bin/${name}" "bin/${name}"
done
npm install --global verdaccio
usrcron="$(mktemp)"
cat > "${usrcron}" <<EOS
35 6  * * *  ${HOME}/bin/npm-update
#
EOS
crontab "${usrcron}"
rm -f "${usrcron}"
cat > "${HOME}/bin/npm-update" <<"EOS"
#!/bin/sh
export PATH="${PATH}:${HOME}/bin"
npm -g update
EOS
chmod u+x "${HOME}/bin/npm-update"

mkdir -p "${HOME}/opt/verdaccio"
cd "${HOME}/opt/verdaccio"
cat > "start-verdaccio" <<"EOS"
#!/bin/sh
export PATH="${PATH}:${HOME}/bin"
exec verdaccio
EOS
chmod u+x start-verdaccio
mkdir storage plugins
touch htpasswd
cat > config.yaml <<"EOS"
storage: ./storage
plugins: ./plugins
web:
  title: Verdaccio
auth:
  htpasswd:
    file: ./htpasswd
uplinks:
  npmjs:
    url: https://registry.npmjs.org/
packages:
  '@*/*':
    access: $all
    publish: $authenticated
    unpublish: $authenticated
    proxy: npmjs
  '**':
    access: $all
    publish: $authenticated
    unpublish: $authenticated
    proxy: npmjs
listen: "0.0.0.0:4873"
server:
  keepAliveTimeout: 60
middlewares:
  audit:
    enabled: true
log: { type: stdout, format: pretty, level: http }
EOS

# Note: gnupg stuff keeps open some files, and then mmdebstrap
# cannot umount cleanly after the customize-script has ended.
# as a stop-gap "solution", just kill the offending processes.
ps -Alf
killall --verbose dirmngr
killall --verbose gpg-agent
ps -Alf
EOF2

chroot "${TARGET_DIR}" /bin/bash <<"EOF2"
cat > /etc/systemd/system/verdaccio.service <<"EOF3" 
[Unit]
Requires=network.target

[Install]
WantedBy=multi-user.target

[Service]
Type=simple
Restart=always
RestartSec=1
ExecStart=/home/user/opt/verdaccio/start-verdaccio
WorkingDirectory=/home/user/opt/verdaccio
User=user
Group=user
EOF3
systemctl enable verdaccio.service
EOF2

sleep 0.5
cat > "${TARGET_DIR}/etc/cron.daily/blindly-autoupgrade" <<"EOF2"
#!/bin/sh
apt-get update -q -q
apt-get full-upgrade -q -q --yes
EOF2
chmod u+x "${TARGET_DIR}/etc/cron.daily/blindly-autoupgrade"
EOF1
chmod a+rx "${CUSTOMIZE_FILE}"

mkdir "${CONTAINER_DIR}"

if [[ -z "${XDG_CONFIG_HOME:-}" ]]; then
    if command -v systemd-path &>/dev/null; then
        XDG_CONFIG_HOME="$(systemd-path user-configuration)"
    else
        XDG_CONFIG_HOME="${HOME}/.config"
    fi
fi

cat > "${CONTAINER_DIR}/config" <<EOF1
# system specific
lxc.include = /usr/share/lxc/config/common.conf
lxc.include = /usr/share/lxc/config/userns.conf
lxc.arch = amd64

# user specific
lxc.include = ${XDG_CONFIG_HOME}/lxc/default.conf

# container specific
lxc.mount.auto = proc:mixed sys:ro cgroup:mixed
lxc.rootfs.path = dir:${CONTAINER_DIR}/rootfs
lxc.uts.name = ${CONTAINER_NAME}
EOF1


mmdebstrap \
    --variant=apt \
    --mode=unshare \
    --format=directory \
    --aptopt="${APTOPT_FILE}" \
    --include="systemd-sysv,ifupdown,isc-dhcp-client" \
    --include="gpgv,gpg,dirmngr,gpg-agent" \
    --include="wget,ca-certificates" \
    --include="xz-utils" \
    --include="cron" \
    --include="procps,psmisc" \
    --customize-hook="echo "${CONTAINER_NAME}" > \"\${1}/etc/hostname\"" \
    --customize-hook="${CUSTOMIZE_FILE}" \
    bookworm \
    "${CONTAINER_DIR}/rootfs"


echo
echo "finished"
echo
echo "the lxc container can now be started with:"
echo
echo "lxc-unpriv-start --name ${CONTAINER_NAME}"
echo
