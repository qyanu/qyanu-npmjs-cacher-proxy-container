#!/usr/bin/bash
set -eu
CONTAINER_NAME="npmjs-cacher-proxy"

TMPDIR="$(mktemp -d)"
cleanup() {
    set +e
    rm -rf --one-file-system "$TMPDIR"
}
trap cleanup EXIT


cat > "${TMPDIR}/Dockerfile" <<"EOF"
FROM verdaccio/verdaccio
EOF

docker build \
    --pull \
    --progress=plain \
    --tag "${CONTAINER_NAME}" \
    "${TMPDIR}" \
    #
