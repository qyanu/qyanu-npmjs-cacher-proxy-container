
*************************************
 Container for NPMJS Registry Cacher
*************************************

This repository provides turnkey-scripts to setup and run either a LXC-
or DOCKER- container on one's personal computer that will provide a
local cache for the npmjs registry.

This local cacher proxy can then be used with npm's environment
variable ``NPM_CONFIG_REGISTRY=http://${NPMJS_CACHER_PROXY_IP_ADDRESS}:4873``.
A script intended to be included in one's ``.bashrc`` is provided.

I use debian/bookworm on amd64, so this is currently the only
environment these scripts are tested. Please contact me regarding
problems in other environments!


Setup/Build Container
---------------------

Use one of the following scripts to setup the respective type
of container:

./setup/lxc-unprivileged-user-container.sh

    This will create an unprivileged user lxc container named
    ``npmjs-cacher-proxy`` that will provide a npm registry on
    its tcp port 80.

    See the script's help for information on the necessary arguments.

./setup/docker-user-container.sh

    This will create a docker use container named
    ``npmjs-cacher-proxy`` that will provide a npm registry on
    its tcp port 80.


Run Container
-------------

Use one of the following scripts to run (start) the ready setup container
when needed:

* ./run/lxc-unprivileged-user-container.sh
* ./run/docker-user-container.sh


Integrate in .bashrc
--------------------

Add the following snippet to ``.bashrc`` in order to automatically
start the container on login and exporting the variable
``NPM_CONFIG_REGISTRY``(assuming bash as the login shell).

For LXC container::

    path/to/qyanu-npmjs-cacher-proxy-container/run/lxc-unprivileged-user-container.sh.sh
    eval "$(path/to/qyanu-npmjs-cacher-proxy-container/bash-integration/lxc-unprivileged-user-container.sh)"

For Docker container::

    path/to/qyanu-npmjs-cacher-proxy-container/run/docker-unprivileged-container.sh
    eval "$(path/to/qyanu-npmjs-cacher-proxy-container/bash-integration/docker-unprivileged-container.sh)"
