#!/usr/bin/bash
set -eu
CONTAINER_NAME="npmjs-cacher-proxy"
if ! lxc-info --name "${CONTAINER_NAME}" &>/dev/null; then
    echo "ERROR: lxc container not found: ${CONTAINER_NAME}" >&2
    exit 1
fi
state="$(lxc-info --name "${CONTAINER_NAME}" --no-humanize --state)"
if [[ "RUNNING" = "$state" ]]; then
    echo "INFO: lxc container already running: ${CONTAINER_NAME}" >&2
    exit 0
fi
exec lxc-unpriv-start --name "${CONTAINER_NAME}"
