#!/usr/bin/bash
set -eu
CONTAINER_NAME="npmjs-cacher-proxy"

exec docker container run \
    --rm \
    -ti \
    --detach \
    --name "${CONTAINER_NAME}" \
    "${CONTAINER_NAME}"
    --listen 0.0.0.0:4873 \
    --background \
    #
